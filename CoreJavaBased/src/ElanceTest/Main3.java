package ElanceTest;


import java.util.Scanner;

public class Main3 {
	public static final int[] HOPE_CONSTANT = {20,10,5};
	
	public static void main(String[] args)
	{
		Scanner in = new Scanner(System.in);
        
		int[] hopeArr = new int[4];
        String line1 = in.nextLine();
        String line2 = in.nextLine();
        String line3 = in.nextLine();
        String line4 = in.nextLine();
        
        if( (line1 != null) && (line2 != null) && (line2 != null) && (line4 != null) )
        {
        	hopeArr[0] = Integer.parseInt(line1);
        	hopeArr[1] = Integer.parseInt(line2);
        	hopeArr[2] = Integer.parseInt(line3);
        	hopeArr[3] = Integer.parseInt(line4);
        	for(int i=0; i<hopeArr.length; i++)
    		{
        		startProcess(hopeArr[i]);
    		}
        	
        	
        }
        
		
	}
	
	private static void startProcess(int hopArr)
	{
		int finalResult = 0;
		for(int i=0, j=0; i<hopArr; i++,j++)
		{
    		if(j >= HOPE_CONSTANT.length)
    		{
    			j=0;
    		}
    		finalResult += HOPE_CONSTANT[j];
		}
		System.out.println(finalResult);
	}

}
