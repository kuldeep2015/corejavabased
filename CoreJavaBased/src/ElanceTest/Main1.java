package ElanceTest;

import java.util.GregorianCalendar;



public class Main1 {
	
	private static final int DATE_28 = 28;
	public static void main(String[] args)
	{
		
		
		if(args.length > 0)
		{
			for (String string : args) {
				startProcess(string);
			}
			
			
		}
		
	}
	
	private static void startProcess(String string)
	{
		String year="";
		String month="";
		String tmpStr = string;
		String[] strArray = tmpStr.split("-");
		if(strArray != null && strArray.length == 2)
		{
			year = strArray[0];
			month = strArray[1];
			
			 
		      GregorianCalendar cal = (GregorianCalendar) GregorianCalendar.getInstance();

		      cal.set(new Integer(year), new Integer(month), DATE_28);
		      
		      int dayOfWeek = cal.get(GregorianCalendar.DAY_OF_WEEK);
		      
		      
		      if( dayOfWeek == GregorianCalendar.SUNDAY)
		      {
		    	  System.out.println("SUNDAY");
		      }
		      else if(dayOfWeek == GregorianCalendar.MONDAY)
		      {
		    	  System.out.println("MONDAY");
		      }
		      else if(dayOfWeek == GregorianCalendar.TUESDAY)
		      {
		    	  System.out.println("TUESDAY");
		      }
		      else if(dayOfWeek == GregorianCalendar.WEDNESDAY)
		      {
		    	  System.out.println("WEDNESDAY");
		      }
		      else if(dayOfWeek == GregorianCalendar.THURSDAY)
		      {
		    	  System.out.println("THURSDAY");
		      }
		      else if(dayOfWeek == GregorianCalendar.FRIDAY)
		      {
		    	  System.out.println("FRIDAY");
		      }
		      else if(dayOfWeek == GregorianCalendar.SATURDAY)
		      {
		    	  System.out.println("SATURDAY");
		      }
		      
		      	
		}
	}

}
