package ElanceTest;


import java.util.GregorianCalendar;
import java.util.Scanner;



public class Main2 {
	
	private static final int DATE_28 = 28;
	
	public static void main(String[] args)
	{
		Scanner in = new Scanner(System.in);
        
        
        String line1 = in.nextLine();
        String line2 = in.nextLine();
        
        
        if(line1 != null)
        {
        	startProcess(line1);
        }
        if(line2 != null)
        {
        	startProcess(line2);
        }
		
	}
	
	private static void startProcess(String string)
	{
		String year="";
		String month="";
		String tmpStr = string;
		String[] strArray = tmpStr.split("-");
		if(strArray != null && strArray.length == 2)
		{
			year = strArray[0];
			month = strArray[1];
			
			 
		      GregorianCalendar cal = (GregorianCalendar) GregorianCalendar.getInstance();

		      cal.set(new Integer(year), new Integer(month), DATE_28);
		      
		      int dayOfWeek = cal.get(GregorianCalendar.DAY_OF_WEEK);
		      
		      
		      if( dayOfWeek == GregorianCalendar.SUNDAY)
		      {
		    	  System.out.println("SUNDAY");
		      }
		      else if(dayOfWeek == GregorianCalendar.MONDAY)
		      {
		    	  System.out.println("MONDAY");
		      }
		      else if(dayOfWeek == GregorianCalendar.TUESDAY)
		      {
		    	  System.out.println("TUESDAY");
		      }
		      else if(dayOfWeek == GregorianCalendar.WEDNESDAY)
		      {
		    	  System.out.println("WEDNESDAY");
		      }
		      else if(dayOfWeek == GregorianCalendar.THURSDAY)
		      {
		    	  System.out.println("THURSDAY");
		      }
		      else if(dayOfWeek == GregorianCalendar.FRIDAY)
		      {
		    	  System.out.println("FRIDAY");
		      }
		      else if(dayOfWeek == GregorianCalendar.SATURDAY)
		      {
		    	  System.out.println("SATURDAY");
		      }
		      
		      	
		}
	}

}

