package ElanceTest;

import java.util.Scanner;

public class Main6 {

	public static final String specialChar = "@#*=";
	public static void main(String[] args)
	{
		Scanner in = new Scanner(System.in);
		String[] emailStr = new String[4];
		
        for(int i=0; i< emailStr.length; i++)
		emailStr[i] = in.nextLine();
     
        for (int i = 0; i < emailStr.length; i++) {
        	validateEmail(emailStr[i]);
		}
        
        
	}
	
	private static void validateEmail(String emailStr)
	{
		emailStr = emailStr.trim();
		if( ( emailStr.indexOf(" ") != -1 ) )
		{
			System.out.println("FAIL");
			return;
		}
		
		if(emailStr.length() < 5 || emailStr.length() > 10)
		{
			System.out.println("FAIL");
			return;
		}
		char[] charArr = emailStr.toCharArray();
		boolean isChar = false;
		boolean isDigit = false;
		boolean isSpecial = false;
		for (int i = 0; i < charArr.length; i++) {
			if(charArr[i] >= 65 && charArr[i] <= 90)
			{
				isChar = true;
			}
			
			if(charArr[i] >= 48 && charArr[i] <= 57)
			{
				isDigit = true;
			}
			
			if(specialChar.indexOf( charArr[i] ) != -1 )
			{
				isSpecial = true;
			}
		}
		
		if(!isChar)
		{
			System.out.println("FAIL");
			return;
		}
		
		if(!isDigit)
		{
			System.out.println("FAIL");
			return;
		}
		
		if(!isSpecial)
		{
			System.out.println("FAIL");
			return;
		}
		
		System.out.println("PASS");
		
	}
}
