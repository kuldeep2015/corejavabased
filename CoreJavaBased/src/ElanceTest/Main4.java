package ElanceTest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

public class Main4 {

	public static void main(String[] args)
	{
		Scanner in = new Scanner(System.in);
        
		String[] studentsRec = new String[10];
        for(int i=0; i< studentsRec.length; i++)
		studentsRec[i] = in.nextLine();
        startProcess(studentsRec);
	}
	
	private static void startProcess(String[] studentsRec)
	{
		Map<String, Integer> map = new HashMap<String, Integer>();
		
		
		for(int i=0; i<studentsRec.length; i++)
		{
			String stuRec = studentsRec[i];
			String[] stuRecArr = stuRec.split("-");
			if(map.containsKey(stuRecArr[0]))
			{
				Integer val = map.get(stuRecArr[0]);
				if( Integer.parseInt( stuRecArr[1] ) > val )
				{
					map.put(stuRecArr[0], Integer.parseInt( stuRecArr[1] ));
				}
			}
			else
			{
				map.put(stuRecArr[0], Integer.parseInt( stuRecArr[1] ));
			}
		}
		
		List<Entry<String, Integer>> listEntries = entriesSortedByValues(map);
		for (Entry<String, Integer> entry : listEntries) {
			System.out.println( entry.getKey() + "-" + entry.getValue());
		}
		
	}
	
	static <K,V extends Comparable<? super V>> 
    List<Entry<K, V>> entriesSortedByValues(Map<K,V> map) {

List<Entry<K,V>> sortedEntries = new ArrayList<Entry<K,V>>(map.entrySet());

Collections.sort(sortedEntries, new Comparator<Entry<K,V>>() {
        @Override
        public int compare(Entry<K,V> e1, Entry<K,V> e2) {
            return e2.getValue().compareTo(e1.getValue());
        }
    }
);

return sortedEntries;
}
}
