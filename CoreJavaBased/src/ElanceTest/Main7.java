package ElanceTest;

import java.util.Scanner;

public class Main7 {

	public static void main(String[] args)
	{
		Scanner in = new Scanner(System.in);
		String[] numberStr = new String[3];
		
        for(int i=0; i< numberStr.length; i++)
		numberStr[i] = in.nextLine();
            
        for (int i = 0; i < numberStr.length; i++) {
        	startProcess(numberStr[i]);
		}
        
        
	}
	
	private static void startProcess(String numberStr)
	{
		int number = Integer.parseInt(numberStr);
		
		createPiramid(number);
		
	}
	
	private static void createPiramid(int baseNumber)
	{
		for (int i = 1; i <= baseNumber; i++) {
			
			for (int k = i; k < baseNumber; k++) {
				System.out.print(" ");
			}
			
			for (int j = 1; j <= i; j++) {
				
				System.out.print(i);
				System.out.print(" ");
				
			}
			System.out.println();
			
		}
	}
}
