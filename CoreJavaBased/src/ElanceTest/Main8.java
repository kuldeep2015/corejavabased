package ElanceTest;

import java.util.Scanner;

public class Main8 {

	public static void main(String[] args)
	{
		Scanner in = new Scanner(System.in);
		
		
        
		String numberStr = in.nextLine();
		String[] strArr = numberStr.split(",");
            
        
        startProcess(strArr[0], strArr[1]);
		
        
        
	}
	
	
	private static void startProcess(String number1, String number2)
	{
		char[] charArr1 = number1.toCharArray();
		char[] charArr2 = number2.toCharArray();
		
		int sumArr1 = 0;
		int sumArr2 = 0;
		for (int i = 0; i < charArr1.length; i++) {
			sumArr1 = sumArr1 + (int)charArr1[i];
		}
		
		for (int i = 0; i < charArr2.length; i++) {
			sumArr2 = sumArr2 + (int)charArr2[i];
		}
		
		System.out.println( (sumArr1 - sumArr2) );
		
	}
}
