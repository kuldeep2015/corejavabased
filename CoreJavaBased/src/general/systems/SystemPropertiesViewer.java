package general.systems;

import java.util.Properties;
import java.util.Set;

public class SystemPropertiesViewer {

	public static void main(String[] args) {
		Properties properties =  System.getProperties();
		Set<Object> set = properties.keySet();
		
		for (Object object : set) {
			String strKey = (String)object;
			String value = properties.getProperty(strKey);
			
			System.out.println("[Key: " + strKey + "]  [Value: " + value + "]");
		}
		
	}
}
