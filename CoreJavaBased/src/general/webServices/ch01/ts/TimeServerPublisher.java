package general.webServices.ch01.ts;

import javax.xml.ws.Endpoint;

public class TimeServerPublisher {
	public static void main(String[] args)
	{
		String url = "http://127.0.0.1:9876/ts";
		Endpoint.publish(url, new TimeServerImpl());
		System.out.println("WebService, TimeServerPublisher published ");
	}
}
