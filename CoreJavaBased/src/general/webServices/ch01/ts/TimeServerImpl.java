package general.webServices.ch01.ts;

import java.util.Date;

import javax.jws.WebService;

@WebService(endpointInterface = "general.webServices.ch01.ts.TimeServer")
public class TimeServerImpl implements TimeServer{
	static long requestNumber = 0;
	
	public TimeServerImpl() {
		// TODO Auto-generated constructor stub
		super();
		System.out.println("TimeServerImpl instance created no., " + ( ++requestNumber) );
	}
	
	@Override
	public String getTimeAsString() {
		// TODO Auto-generated method stub
		return new Date().toString();
	}
	
	@Override
	public long getTimeAsElapsed() {
		// TODO Auto-generated method stub
		return new Date().getTime();
	}
}
