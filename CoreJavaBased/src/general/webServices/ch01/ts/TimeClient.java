package general.webServices.ch01.ts;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

public class TimeClient {
	public static void main(String[] args) throws Exception
	{
		URL url = new URL("http://localhost:9876/ts?wsdl");
		//Qualified name of the service:
		// 1st arg is the service URI
		// 2nd is the service name published in the WSDL
		QName qName = new QName("http://ts.ch01.webServices.general/","TimeServerImplService");
		// Create, in effect, a factory for the service.
		Service service = Service.create(url, qName);
		// Extract the endpoint interface, the service port
		TimeServer eif = service.getPort(TimeServer.class);
		System.out.println("eif.getTimeAsString(): " + eif.getTimeAsString());
		System.out.println("eif.getTimeAsElapsed(): " + eif.getTimeAsElapsed());
		
	}
}
