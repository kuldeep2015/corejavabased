package general.testing.exceptions;

public class TestException {
	
	public static void main(String[] args) throws TestCompileTimeException
	{
		TestException testException = new TestException();
		testException.tryCompileTimeExceptionMethod();
		
		//testException.tryRuntimeExceptionMethod();
	}
	
	void tryCompileTimeExceptionMethod() throws TestCompileTimeException
	{
		System.out.println("In tryCompileTimeExceptionMethod()");
		try {
			throw new TestCompileTimeException();
		} catch (TestCompileTimeException e) {
			// TODO Auto-generated catch block
			System.out.println("tryCompileTimeExceptionMethod() exception caught");
			//e.printStackTrace();
			
			throw e;
		}
	}
	
	void tryRuntimeExceptionMethod() throws TestRuntimeException
	{
		System.out.println("In tryRuntimeExceptionMethod()");
		try
		{
			throw new TestRuntimeException();
		}
		catch(TestRuntimeException tre)
		{
			System.out.println("tryRuntimeExceptionMethod() caught");
			throw tre;
		}
		
		
	}

}
