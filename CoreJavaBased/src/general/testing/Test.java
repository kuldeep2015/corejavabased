package general.testing;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;


/*
 * 
 */
public class Test {
	
	public final static Logger logger = Logger.getAnonymousLogger();
	
	public static void main(String[] args)
	{
		String newObject = new String("Hello");
		System.out.println("New Object Hello:" + newObject.hashCode());
		String copyString = "Hello";
		System.out.println("New Object Hello:" + copyString.hashCode());
		String newString = new String("Hello");
		System.out.println("New Object Hello:" + newString.hashCode());
		String copyString2 = "Hello";
		System.out.println("New Object Hello:" + copyString2.hashCode());
		
		if(newObject == copyString)
			System.out.println("newObject == copyString");
		else
			System.out.println("newObject != copyString");
		
		if(copyString == copyString2)
			System.out.println("copyString == copyString2");
		else
			System.out.println("copyString != copyString2");
		
		Set<String> hashSet = new HashSet<String>();
		hashSet.add("Hello");
		
//		while (true) {
//			try {
//				Thread.sleep(3000);
//				logger.log(Level.INFO, "Executing");
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
	}
	
	public static void testStatic()
	{
		List<Integer> list;
		int SIZE = 10;
		
		PrintWriter out = null;

		try {
			System.out.println("Entered try statement");
		    list = new ArrayList<Integer>();
			out = new PrintWriter(new FileWriter("OutFile.txt"));
		    for (int i = 0; i < SIZE; i++) {
		        out.println("Value at: " + i + " = " + list.get(i));
		    }
		}
		catch(FileNotFoundException ex)
		{
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
	}
	

}
