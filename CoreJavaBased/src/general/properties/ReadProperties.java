package general.properties;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class ReadProperties {
	
	public static void main(String[] args) {
		
		String FILE_PATH_A = "D://workspace/CoreJavaBased/A.config";
		String FILE_PATH_B = "D://workspace/CoreJavaBased/B.config";
		
		ReadProperties readProperties = new ReadProperties();
		Properties properties = readProperties.loadProperties(FILE_PATH_A);
		properties.put("New1", "New1");
		properties.put("Put2", "Put2");
		
		System.out.println(properties);
		
		try {
			properties.store(new FileOutputStream("Mix.config"), "Hello1");
			properties.storeToXML( new FileOutputStream("Mix.xml"), "Hello1");
			
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private Properties loadProperties(String filePath)
	{
		Properties properties = new Properties();
		try {
			properties.load( new FileInputStream(new File(filePath)));
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return properties;
		
	}
}
