package general.properties;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 * @author kuldeep
 * When loading Property File with ClassLoader as 
 * TestLoadProperties.class.getClassLoader().getResourceAsStream(TestLoadProperties.CONFIG_FILE) then the property file must be in the root folder of the class file.
 * and if Propert file is to be loaded without calling getClassLoader() like
 * TestLoadProperties.class.getResourceAsStream(TestLoadProperties.CONFIG_FILE) then Property file must be in the same directory where the class file is.
 *
 */
public class TestLoadProperties {

	final static String CONFIG_FILE = "A.config";  
	final static Properties properties = new Properties();
	final static Logger logger = Logger.getLogger(TestLoadProperties.class.getName());
	
	static {
		try {
			// Needed propert file to be in the root folder of the class file. 
			InputStream inputStream = TestLoadProperties.class.getClassLoader().getResourceAsStream(TestLoadProperties.CONFIG_FILE);
			
			// Needed propert file to be in the same folder of the class file.
			//InputStream inputStream = TestLoadProperties.class.getResourceAsStream(TestLoadProperties.CONFIG_FILE);
			
			
			// load the inputStream using the Properties
			properties.load(inputStream);
		} catch (IOException e) {
			logger.log(Level.INFO,"Error in loading properties file.", e);
			throw new RuntimeException(e);
		}
	}
	
	public static void main(String[] args)
	{
		Enumeration<Object> enumeration =  properties.elements();
		while( enumeration.hasMoreElements() )
		{
			String element = (String)enumeration.nextElement();
			logger.log(Level.INFO,"Element: " + element);
		}
		
		System.out.println("TestLoadProperties.class.getCanonicalName(): " + TestLoadProperties.class.getCanonicalName());
		
		//new TestLoadProperties().loadPropertyFileFromOtherWay();
	}
	
	// If using this method then Property file must be in same folder where this java class file exists.
	private void loadPropertyFileFromOtherWay()
	{
		/*try {
			InputStream inputStream = getClass().getResourceAsStream(TestLoadProperties.CONFIG_FILE);
			// load the inputStream using the Properties
			properties.load(inputStream);
		} catch (IOException e) {
			logger.log(Level.INFO,"Error in loading properties file.", e);
			throw new RuntimeException(e);
		}*/
		
		System.out.println("getClass().getCanonicalName(): " + getClass().getCanonicalName());
	}
}
