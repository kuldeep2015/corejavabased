package general.generics;

import java.util.ArrayList;
import java.util.List;

class CreateAnArrayList {
	public <T> List<T> makeArrayList(T t) // Take an object of unknown type and use a "T" to represent the type
	{
		List<T> list = new ArrayList<T>();
		list.add(t);
		return list;
	}
	
}

public class UseAnArrayList{
	
	public static void main(String[] args) {
		Bird bird = new Bird();
		CreateAnArrayList createAnArrayList = new CreateAnArrayList();
		List<Bird> birdList = createAnArrayList.makeArrayList(bird);
		
	}
}

class Bird{
	
}

