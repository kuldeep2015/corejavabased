package general.generics;

class Generics1<T> {		// As the class type
	T anInstance;				// As an instance variable type
	T[] anArrayOfTs;			// As an array type
	
	public Generics1(T anInstance) {	// As an argument type
		this.anInstance = anInstance;
	}
	
	T getT()	//As an return type
	{
		return anInstance;
	}
}

public class UseGenerics1{
	public static void main(String[] args) {
		Dog dog = new Dog();
		Generics1<Dog> generics1 = new Generics1<Dog>(dog);
		Dog dogNew = generics1.getT();
		 
	}
}

class Dog{
	
}

class Mouse{
	
}