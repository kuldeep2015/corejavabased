package general.generics;

import java.util.ArrayList;
import java.util.List;

public class TestRental {
	
	public static void main(String[] args) {
		//make some Cars for the pool
		Car car1 = new Car();
		Car car2 = new Car();
		List<Car> carList = new ArrayList<Car>();
		carList.add(car1);
		carList.add(car2);
		
		RentalGeneric<Car> carRental = new RentalGeneric<Car>(2, carList);
		//Now get a Car out it won't need a cast
		Car carToRent = carRental.getRental();
		carRental.returnRental(carToRent);
		
	}
}

class Car
{
	
}