package general.generics;

public class UseTwo<T, X> {
	T one;
	X two;
	
	public UseTwo(T one, X two) {
		this.one = one;
		this.two = two;
		System.out.println("UseTwo initialized with one: " + this.one + " and two: " + this.two );
	}
	
	T getT()
	{
		return one;
	}
	
	X getX()
	{
		return two;
	}
	
	//Test it by creating it with <String, Integer>
	
	public static void main(String[] args) {
		UseTwo<String, Integer> twos = new UseTwo<String, Integer>("foo", 42);
		String theT = twos.getT();
		int theX = twos.getX();
		
		
	}
}
