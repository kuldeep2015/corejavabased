package general.generics;

import java.util.List;

public class RentalGeneric<T> {
	private List<T> rentalPool;
	private int maxNum;
	
	public RentalGeneric(int maxNum, List<T> rentalPool)
	{
		this.maxNum = maxNum;
		this.rentalPool = rentalPool;
		System.out.println("Rental generic initialized with maxNum:" + this.maxNum + " and rentalPool: " + this.rentalPool );
	}
	
	public T getRental()
	{
		T car = rentalPool.get(0);
		System.out.println("getting carRental: " + car);
		return car;
	}
	
	public void returnRental(T returnedThing)
	{
		System.out.println("returning carRental: " + returnedThing);
		rentalPool.add(returnedThing);
	}
	
}
