package designPatterns.strategyPattern.simUDuckExample;

public class FlyNoWayImpl implements FlyBehavior {

	@Override
	public void fly() {
		// TODO Auto-generated method stub
		System.out.println("fly() overridden in FlyNoWay");
	}
}
