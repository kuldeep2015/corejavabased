package designPatterns.strategyPattern.simUDuckExample;

public class ModelDuck extends Duck {

	public ModelDuck()
	{
		flyBehavior = new FlyNoWayImpl();
		quackBehavior = new QuackImpl();
	}
	
	@Override
	public void display() {
		// TODO Auto-generated method stub
		System.out.println("display() in ModelDuck");
	}

}
