package designPatterns.strategyPattern.simUDuckExample;

public class MallardDuck extends Duck{

	public MallardDuck()
	{
		quackBehavior = new QuackImpl();
		flyBehavior = new FlyWithWingsImpl();
	}
	
	@Override
	public void display() {
		// TODO Auto-generated method stub
		System.out.println("display() in MallardDuck");
	}
}
