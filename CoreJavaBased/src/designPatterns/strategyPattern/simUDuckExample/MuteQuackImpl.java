package designPatterns.strategyPattern.simUDuckExample;

public class MuteQuackImpl implements QuackBehavior{

	@Override
	public void quack() {
		// TODO Auto-generated method stub
		System.out.println("quack() implemented in MuteQuackImpl");
	}
}
