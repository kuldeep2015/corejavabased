package designPatterns.strategyPattern.simUDuckExample;

public abstract class Duck {

	FlyBehavior flyBehavior;
	QuackBehavior quackBehavior;
	
	public abstract void display();
	
	public void swim()
	{
		System.out.println("swim() of Duck");
	}
	
	public void performQuack()
	{
		System.out.println("performQuack() of Duck");
		quackBehavior.quack();
	}
	
	public void performFly()
	{
		System.out.println("performFly() of Duck");
		flyBehavior.fly();
	}
	
	public void setFlyBehavior(FlyBehavior flyBehavior)
	{
		System.out.println("setFlyBehavior() of Duck");
		this.flyBehavior = flyBehavior;
	}
	
	public void setQuackBehavior(QuackBehavior quackBehavior)
	{
		System.out.println("setQuackBehavior() of Duck");
		this.quackBehavior = quackBehavior;
	}
}
