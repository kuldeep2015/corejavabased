package designPatterns.strategyPattern.simUDuckExample;

public interface FlyBehavior {
	
	void fly();
	
}
