package designPatterns.strategyPattern.simUDuckExample;

public interface QuackBehavior {
	
	void quack();

}
