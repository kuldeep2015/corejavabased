package designPatterns.strategyPattern.gameExample;

public abstract class Character {
	
	WeaponBehaviour weaponBehaviour;
	
	public abstract void fight();
	
	public void setWeapon(WeaponBehaviour weaponBehaviour) {
		this.weaponBehaviour = weaponBehaviour;
	}
	
	public void performUseWeapon()
	{
		weaponBehaviour.useWeapon();
	}
	
}
