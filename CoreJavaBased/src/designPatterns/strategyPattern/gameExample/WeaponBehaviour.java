package designPatterns.strategyPattern.gameExample;

public interface WeaponBehaviour {
	void useWeapon();
	
}
