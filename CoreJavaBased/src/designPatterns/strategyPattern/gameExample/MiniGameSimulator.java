package designPatterns.strategyPattern.gameExample;

public class MiniGameSimulator {

	public static void main(String[] args)
	{
		Character character = new King();
		
		character.setWeapon(new SwordBehaviourImpl());
		character.fight();
		character.performUseWeapon();
		
		
		character.setWeapon(new BowAndArraowBehaviourImpl());
		//character.fight();
		character.performUseWeapon();
		
		
		character.setWeapon(new KnifeBehaviourImpl());
		//character.fight();
		character.performUseWeapon();
		
		character = new Queen();
		
		character.setWeapon(new SwordBehaviourImpl());
		character.fight();
		character.performUseWeapon();
		
		
		character.setWeapon(new BowAndArraowBehaviourImpl());
		//character.fight();
		character.performUseWeapon();
		
		
		character.setWeapon(new KnifeBehaviourImpl());
		//character.fight();
		character.performUseWeapon();
		
		character = new Knight();
		
		character.setWeapon(new SwordBehaviourImpl());
		character.fight();
		character.performUseWeapon();
		
		
		character.setWeapon(new BowAndArraowBehaviourImpl());
		//character.fight();
		character.performUseWeapon();
		
		
		character.setWeapon(new KnifeBehaviourImpl());
		//character.fight();
		character.performUseWeapon();
	}
}
