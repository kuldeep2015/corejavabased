package pattern;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternMatcher {

	static Pattern digitPattern = Pattern.compile( "[0-9]");
	static Pattern letterPattern = Pattern.compile( "[a-zA-Z]");
	static Pattern specialCharsDefaultPattern = Pattern.compile( "[!@#$]");
	
	static String START_MSISDN = "<msisdn>";
	static String END_MSISDN = "<msisdn/>";
	
	
	static String myString = "!Deep007"; 
	//static String myString = "1A!@#$";
	
	public static void main(String[] args)
	{
		PatternMatcher patternMatcher = new PatternMatcher();
		//patternMatcher.testPattern1();
		//patternMatcher.digitsSerachByMetaChar();
		//patternMatcher.whiteSpaceCharSearchByMetaChar();
		//patternMatcher.wordCharSearchByMetaChar();
		//patternMatcher.vowelsInsensitiveSearch();
		//patternMatcher.searchFromAnyChar();
		//patternMatcher.searchFromCharRange();
		//patternMatcher.searchOneCharWithDigit();
		//patternMatcher.searchSingleDigitHexaNumber();
		patternMatcher.searchMsisdn();
	}
	
	/**
	 * \d MetaChar is used to search the digits only from a source.
	 */
	void digitsSerachByMetaChar()
	{
		System.out.println("Executing mathod: digitsSerachByMetaChar()");
		
		Pattern pattern = Pattern.compile("\\d");
		Matcher matcher = pattern.matcher("a12c3e456f");
		
		while(matcher.find())
		{
			System.out.print(matcher.start());
		}
		
	}
	
	void whiteSpaceCharSearchByMetaChar()
	{
		System.out.println("\nExecuting mathod: whiteSpaceCharSearchByMetaChar()");
		Pattern pattern = Pattern.compile("\\s");
		Matcher matcher = pattern.matcher("a 1 56 _z");
		
		while(matcher.find())
		{
			System.out.print(matcher.start());
		}
	}
	
	/**
	 * Word search contains  Letter,Digits or _ (underscore)
	 */
	void wordCharSearchByMetaChar()
	{
		System.out.println("\nExecuting mathod: wordCharSearchByMetaChar()");
		Pattern pattern = Pattern.compile("\\w");
		Matcher matcher = pattern.matcher("a 1 56 _z");
		
		while(matcher.find())
		{
			System.out.print(matcher.start());
		}	
	}
	
	void vowelsInsensitiveSearch()
	{
		System.out.println("\nExecuting mathod: vowelsInsensitiveSearch()");
		Pattern pattern = Pattern.compile("[aeiouAEIOU]");
		Matcher matcher = pattern.matcher("abcAdeoIOpurU");
		while( matcher.find() )
		{
			System.out.print("[" +  matcher.start() + ",");
			System.out.print(matcher.toMatchResult().group() + "],") ;
		}
	}
	
	void searchFromAnyChar()
	{
		System.out.println("\nExecuting mathod: searchFromAnyChar()");
		//Any one of these will be searched
		Pattern pattern = Pattern.compile("[abc]");
		Matcher matcher = pattern.matcher("aAdeboaIOpCurU");
		while(matcher.find())
		{
			System.out.print("[" +  matcher.start() + ",");
			System.out.print(matcher.toMatchResult().group() + "],") ;
		}
	}
	
	void searchFromCharRange()
	{
		System.out.println("\nExecuting mathod: searchFromCharRange()");
		Pattern pattern = Pattern.compile("[a-f]");
		Matcher matcher = pattern.matcher("aAdeboaIOpCurU");
		
		while(matcher.find())
		{
			System.out.print("[" +  matcher.start() + ",");
			System.out.print(matcher.toMatchResult().group() + "],") ;
		}
	}
	
	void searchOneCharWithDigit()
	{
		// Will search pattern like <p7/> (<[word][digit]/>)
		System.out.println("\nExecuting mathod: searchFromCharRange()");
		Pattern pattern = Pattern.compile("<[a-zA-Z][\\d]/>");
		Matcher matcher = pattern.matcher("aAdeboa9IO<p5/>CurU");
		
		while(matcher.find())
		{
			System.out.print("[" +  matcher.start() + ",");
			System.out.print(matcher.toMatchResult().group() + "],") ;
		}
	}
	
	void searchSingleDigitHexaNumber()
	{
		// Will search pattern like 0x1,0Xf
		System.out.println("\nExecuting mathod: searchSingleDigitHexaNumber()");
		Pattern pattern = Pattern.compile("0[xX][0-9a-fA-F]");
		
		Matcher matcher = pattern.matcher("12 0X 0X12 0Xf 0xg");
		while(matcher.find())
		{
			System.out.print("[" +  matcher.start() + ",");
			System.out.print(matcher.toMatchResult().group() + "],") ;
		}
		
	}
	
	void searchMsisdn()
	{
		System.out.println("\nExecuting mathod: searchMsisdn()");
		
		Pattern pattern = Pattern.compile("<msisdn>(.)*<msisdn/>");
		Matcher matcher = pattern.matcher("<msisdn>919971911149<msisdn/><SERVICE TYPE>PREPAID<SERVICE TYPE/>");
		
		while(matcher.find())
		{
			System.out.print("[" +  matcher.start() + ",");
			System.out.print(matcher.toMatchResult().group() + "],") ;
			extractAndShowMsisdn(matcher.toMatchResult().group());
		}
		
	}
	
	void extractAndShowMsisdn(String msisdnHolderStr)
	{
		System.out.println("\nExecuting mathod: extractAndShowMsisdn(String msisdnHolderStr)");
		
		Pattern pattern = Pattern.compile("\\d+[-]?\\d+");
		Matcher matcher = pattern.matcher(msisdnHolderStr);
		
		while(matcher.find())
		{
			System.out.print("[" +  matcher.start() + ",");
			System.out.print(matcher.toMatchResult().group() + "],") ;
		}
	}
	
	private void testPattern1()
	{
		Matcher digitMatcher = digitPattern.matcher(myString);
		Matcher letterMatcher = letterPattern.matcher(myString);
		Matcher specialCharsDefaultMatcher = specialCharsDefaultPattern.matcher(myString);
	
		if(!digitMatcher.find())
		{
			System.out.println("Digital Matcher find() failed");
		}
		else if (!letterMatcher.find()) {
			System.out.println("Letter Matcher find() failed");
		}
		else if (!specialCharsDefaultMatcher.find()) {
			System.out.println("Special Char Matcher find() failed");
		}
		else
		{
			System.out.println("All is Well");
		}
	
		
		if(!digitMatcher.matches())
		{
			System.out.println("Digital Matcher matches() failed");
		}
		else if (!letterMatcher.matches()) {
			System.out.println("Letter Matcher matches() failed");
		}
		else if (!specialCharsDefaultMatcher.matches()) {
			System.out.println("Special Char Matcher matches() failed");
		}
		else
		{
			System.out.println("All is Well");
		}
	}
}
