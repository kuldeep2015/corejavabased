package testPrograms;

public class Class1 {

	
	public static void main(String[] args) {
		
		new Class1().executeMethod2();

	}
	
	public void executeMethod1()
	{
		try
		{
			System.out.println("Step 1 try executed.");
			throw new Exception("Dummy exception.");
		}
		catch(Exception e)
		{
			System.out.println("Step 2 catch executed.");
			return;
			
		}
		finally
		{
			System.out.println("Step 3 finally executed.");
			return;
		}
	}
	
	public void executeMethod2()
	{
		try
		{
			System.out.println("executeMethod2 Step 1 try executed.");
			throw new Exception("Dummy exception.");
		}
		finally
		{
			System.out.println("executeMethod2 Step 3 finally executed.");
			return;
		}
	}
	
	
	public void executeMethod3()
	{
		try
		{
			System.out.println("executeMethod3 Step 1 try executed.");
			
		}
		finally
		{
			System.out.println("executeMethod3 Step 3 finally executed.");
			
		}
	}
}

