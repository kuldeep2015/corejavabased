package charactersAndStrings;

public class CharacterTypeIdentifier {

	private static StringBuilder stringBuilder = null;
	private static String SEPERATOR = "\n"; 
	public static void main(String[] args)
	{
		String str = "abCdE5	6G7*" +
				"$.()#[]+*&^%| \\ /-@";
		char[] charArray =  str.toCharArray();
		
		for (char c : charArray) {
			stringBuilder = new StringBuilder();
			//System.out.println("Checking for character: " + c);
			updateLog(stringBuilder , "Checking for character: " + c, SEPERATOR);
			getCharType(stringBuilder , c);
			
			showCharType(c);
			System.out.println("*************************************************      *****************************************");
		}
		
		
	}
	
	private static void getCharType(StringBuilder stringBuilder , char c)
	{

		if( Character.isDigit(c) )
		{
			updateLog(stringBuilder , "Character: '" + c +  "' is a digit.", SEPERATOR);
			
		}
		else
		{
			updateLog(stringBuilder , "Character: '" + c +  "' is not a digit.", SEPERATOR);
			
			
			if(Character.isLetter(c))
			{
				updateLog(stringBuilder , "Character: '" + c +  "' is a Letter.", SEPERATOR);
				
				if(Character.isLowerCase(c))
				{
					updateLog(stringBuilder , "Character: '" + c +  "' is a LowerCase Letter.", SEPERATOR);
					
				}
				else
				{
					updateLog(stringBuilder , "Character: '" + c +  "' is a UpperCase Letter.", SEPERATOR);
					
				}
			}
			else if(Character.isWhitespace(c))
			{
				updateLog(stringBuilder , "Character: '" + c +  "' is a White Space Character.", SEPERATOR);
				
				if(Character.isSpaceChar(c))
				{
					
					updateLog(stringBuilder , "Character: '" + c +  "' is a Space Character.", SEPERATOR);
					
					if(Character.SPACE_SEPARATOR == c)
					{
						updateLog(stringBuilder , "Character: '" + c +  "' is a Space Separator Character.", SEPERATOR);
						
					}
					else if(Character.LINE_SEPARATOR == c)
					{
						updateLog(stringBuilder , "Character: '" + c +  "' is a Line Separator Character.", SEPERATOR);
						
					}
					else if(Character.PARAGRAPH_SEPARATOR == c)
					{
						updateLog(stringBuilder , "Character: '" + c +  "' is a Paragraph Separator Character.", SEPERATOR);
						
					}
				}
			}
			else
			{
				updateLog(stringBuilder , "Character: '" + c +  "' is not a White Space Character.", SEPERATOR);
				
			}
			
			
		}
		
		System.out.println(stringBuilder.toString());
		stringBuilder.delete(0, stringBuilder.length());
		//System.out.println("-------------------------------------------------------");
	}
	
	private static void updateLog(StringBuilder stringBuilder , String message, String separator)
	{
		stringBuilder.append(message);
		stringBuilder.append(separator);
	}
	
	
	private static void showCharType(char c)
	{
		switch (Character.getType(c)) {
		case Character.COMBINING_SPACING_MARK:
			printCharType("Character.COMBINING_SPACING_MARK");
			break;
		case Character.CONNECTOR_PUNCTUATION:
			printCharType("Character.CONNECTOR_PUNCTUATION");
			break;
		case Character.CONTROL:
			printCharType("Character.CONTROL");
			break;
		case Character.CURRENCY_SYMBOL:
			printCharType("Character.CURRENCY_SYMBOL");
			break;
		case Character.DASH_PUNCTUATION:
			printCharType("Character.DASH_PUNCTUATION");
			break;
		case Character.DECIMAL_DIGIT_NUMBER:
			printCharType("Character.DECIMAL_DIGIT_NUMBER");
			break;
		case Character.ENCLOSING_MARK:
			printCharType("Character.ENCLOSING_MARK");
			break;
		case Character.END_PUNCTUATION:
			printCharType("Character.END_PUNCTUATION");
			break;
		case Character.FINAL_QUOTE_PUNCTUATION:
			printCharType("Character.FINAL_QUOTE_PUNCTUATION");
			break;
		case Character.FORMAT:
			printCharType("Character.FORMAT");
			break;
		case Character.INITIAL_QUOTE_PUNCTUATION:
			printCharType("Character.INITIAL_QUOTE_PUNCTUATION");
			break;
		case Character.LETTER_NUMBER:
			printCharType("Character.LETTER_NUMBER");
			break;
		case Character.LINE_SEPARATOR:
			printCharType("Character.LINE_SEPARATOR");
			break;
		case Character.LOWERCASE_LETTER:
			printCharType("Character.LOWERCASE_LETTER");
			break;
		case Character.MATH_SYMBOL:
			printCharType("Character.MATH_SYMBOL");
			break;
		case Character.MODIFIER_LETTER:
			printCharType("Character.MODIFIER_LETTER");
			break;
		case Character.MODIFIER_SYMBOL:
			printCharType("Character.MODIFIER_SYMBOL");
			break;
		case Character.NON_SPACING_MARK:
			printCharType("Character.NON_SPACING_MARK");
			break;
		case Character.OTHER_LETTER:
			printCharType("Character.OTHER_LETTER");
			break;
		case Character.OTHER_NUMBER:
			printCharType("Character.OTHER_NUMBER");
			break;
		case Character.OTHER_PUNCTUATION:
			printCharType("Character.OTHER_PUNCTUATION");
			break;
		case Character.OTHER_SYMBOL:
			printCharType("Character.OTHER_SYMBOL");
			break;
		case Character.PARAGRAPH_SEPARATOR:
			printCharType("Character.PARAGRAPH_SEPARATOR");
			break;
		case Character.PRIVATE_USE:
			printCharType("Character.PRIVATE_USE");
			break;
		case Character.SPACE_SEPARATOR:
			printCharType("Character.SPACE_SEPARATOR");
			break;
		case Character.START_PUNCTUATION:
			printCharType("Character.START_PUNCTUATION");
			break;
		case Character.SURROGATE:
			printCharType("Character.SURROGATE");
			break;
		case Character.TITLECASE_LETTER:
			printCharType("Character.TITLECASE_LETTER");
			break;
		case Character.UNASSIGNED:
			printCharType("Character.UNASSIGNED");
			break;
		case Character.UPPERCASE_LETTER:
			printCharType("Character.UPPERCASE_LETTER");
			break;
		default:
			printCharType("Default one !!, No Matching Found");
			break;
		}
		
	}
	
	private static void printCharType(String message)
	{
		System.out.println(message);
		//System.out.println("---------------------------------");
	}
	
}
