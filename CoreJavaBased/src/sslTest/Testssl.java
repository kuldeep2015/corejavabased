package sslTest;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.URL;
import java.security.KeyStore;
import java.security.SecureRandom;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;

import org.apache.commons.codec.binary.Base64;

public class Testssl {

public static void main(String args[]) {
		
		String data = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><methodCall><methodName>Subscription.CreateSubscription</methodName><params><param><value><struct><member><name>MSISDN</name><value><string>9171057147</string></value></member><member><name>ProductName</name><value><string>ONE97HUM</string></value></member><member><name>TransactionID</name><value><string>123456789</string></value></member><member><name>ChannelType</name><value><string>WAP</string></value></member><member><name>ChannelID</name><value><string>R1111</string></value></member><member><name>lineItemID</name><value><string>L1212</string></value></member><member><name>additionalAttributes</name><value><struct><member><name>ServiceAttribute</name></member><member><name>Song</name></member></struct></value></member></struct></value></param></params></methodCall>";
		
		String username = "admin";
		String password = "admin";
		
		String urlAddress = "https://180.178.28.61:443/xmlrpc/";
		
		Base64 base64 = new Base64();
		byte[] bytes = base64.encode((username + ":" + password).getBytes());
		String encodedUsernamePassword = new String(bytes);
		
		String returnString = null;
		try {
			System.out.println(urlAddress);
			URL url = new URL(urlAddress);
			
			HostnameVerifier hv = new HostnameVerifier() {
				public boolean verify(String urlHostName, SSLSession session) {
					if (urlHostName.equals(session.getPeerHost()))
						System.out.println("Verified" + session.getPeerHost()
								+ ">>>>" + urlHostName);
					else
						System.out.println("Warning: URL host '" + urlHostName
								+ " is different to SSLSession host "
								+ session.getPeerHost() + "'.");
					return true;
				}
			};
			HttpsURLConnection.setDefaultHostnameVerifier(hv);
			
			
			HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
//			System.setProperty("javax.net.ssl.trustStore","/usr/java/jdk1.6.0_10/jre/bin");
			
			
			
//			urlConnection.setSSLSocketFactory(getFactory(new File("/usr/java/jdk1.6.0_10/jre/bin/truststore"), "uninor123"));
			urlConnection.setDoOutput(true);
			urlConnection.setDoInput(true);
			urlConnection.setRequestMethod("POST");
			urlConnection.setRequestProperty("Connection", "Keep-Alive");
			urlConnection.setRequestProperty("Keep-Alive", "header");
			urlConnection.setRequestProperty("Content-Type", "text/xml");
			urlConnection.setRequestProperty("Authorization", "Basic " + encodedUsernamePassword);
			
			System.out.println("" + data.getBytes().length);
			OutputStream out = urlConnection.getOutputStream();
			Writer wout = new OutputStreamWriter(out);
			wout.write(data);
			wout.flush();
			wout.close();
			InputStream in = urlConnection.getInputStream();
			DataInputStream ds = new DataInputStream(in);
			System.out.println(ds.available());
			byte[] returnData = new byte[ds.available()];
			ds.read(returnData);
			returnString = new String(returnData);
			in.close();
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println(ex);
		}
		
		System.out.println(returnString);
	} // end main
}
