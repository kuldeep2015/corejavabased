package tryCatchFinally;

public class TestFinally {

	
	public static void main(String[] args)
	{
		try {
			new TestFinally().testTryCatshFinally();
		} catch (Exception e) {
			
			
			System.out.println("Exception caught in Main");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	void testTryCatshFinally() throws Exception
	{
		try
		{
			System.out.println("In Try");
			throw new Exception("Test Exdception");
		}
		catch(Exception ex)
		{
			System.out.println("In Catch");
			throw ex;
		}
		finally
		{
			System.out.println("In finally");
		}
		
	}
	
}
