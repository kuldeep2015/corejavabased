package rmiTest.clientSide;

import java.awt.Button;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowListener;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import rmiTest.serverSide.RMIInterface;

public class RMIClient extends Frame implements ActionListener {

	Button btnAdd,btnSub,btnCancel;
	TextField txtFirst,txtSecond,txtResult;
	float first,second,result;
	RMIInterface remote;
	
	public RMIClient()
	{
		Label lblFirst = new Label("Enter first Number");
		Label lblSecond = new Label("Enetr second Number");
		Panel p1 = new Panel();
		txtFirst = new TextField(15);
		p1.add(txtFirst);
		
		Panel p2 = new Panel();
		txtSecond = new TextField(15);
		p2.add(txtSecond);
		
		Panel p3 = new Panel();
		btnAdd = new Button("ADD");
		btnAdd.addActionListener(this);
		p3.add(btnAdd);
		
		Panel p4 = new Panel();
		btnSub = new Button("SUB");
		btnSub.addActionListener(this);
		p4.add(btnSub);
		
		Label lblResult = new Label("RESULT IS:");
		
		Panel p5 = new Panel();
		txtResult = new TextField(15);
		p5.add(txtResult);
		
		setLayout(new GridLayout(4,2));
		
		add(lblFirst);
		add(p1);
		add(lblSecond);
		add(p2);
		add(p3);
		add(p4);
		add(lblResult);
		add(p5);
		
		setSize(400,200);
		setVisible(true);
		setLocation(200,200);
		
		try {
			remote = (RMIInterface)Naming.lookup("rmi://localhost:1099/ProxyName_Test");
			float result = remote.add(10.5f, 5.4f);
			System.out.println("Result is: " + result);
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	@Override
	public void actionPerformed(ActionEvent eve) {
		// TODO Auto-generated method stub
		try
		{
			first = Float.valueOf(txtFirst.getText()).floatValue();
			second = Float.valueOf(txtSecond.getText()).floatValue();
			
			if(eve.getSource() == btnAdd)
			{
				result = remote.add(first, second);
				String strq = String.valueOf(result);
				txtResult.setText(strq);
				
			}
			
			if(eve.getSource() == btnSub)
			{
				result = remote.sub(first, second);
				String strq = String.valueOf(result);
				txtResult.setText(strq);
			}
		}
		catch(Exception ex)
		{
			
		}		
	}
	
	public static void main(String[] args)
	{
		RMIClient rmiClient = new RMIClient();
	}
}
