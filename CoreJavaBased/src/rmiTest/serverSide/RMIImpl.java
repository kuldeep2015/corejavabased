package rmiTest.serverSide;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class RMIImpl extends UnicastRemoteObject implements RMIInterface {

	public RMIImpl()throws RemoteException
	{
		System.out.println("REMOTE OBJECT INSTANTIATED");
	}
	
	@Override
	public float add(float firstNumber, float secondNumber)
			throws RemoteException {
		// TODO Auto-generated method stub
		float result = firstNumber+secondNumber;
		return result;
	}

	@Override
	public float sub(float firstNumber, float secondNumber)
			throws RemoteException {
		// TODO Auto-generated method stub
		float result = firstNumber-secondNumber;
		return result;
	}
	
	

}
