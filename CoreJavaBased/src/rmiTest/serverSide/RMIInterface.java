package rmiTest.serverSide;

import java.rmi.Remote;
import java.rmi.RemoteException;


public interface RMIInterface extends Remote{

	public float add(float firstNumber, float secondNumber)throws RemoteException;
	
	public float sub(float firstNumber, float secondNumber)throws RemoteException;
	
}
