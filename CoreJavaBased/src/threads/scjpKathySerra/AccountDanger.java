package threads.scjpKathySerra;

public class AccountDanger implements Runnable {

	private Account acct = new Account();
	
	public static void main(String[] args)
	{
		AccountDanger accountDanger = new AccountDanger();
		Thread one = new Thread(accountDanger);
		Thread two = new Thread(accountDanger);
		one.setName("Fred");
		two.setName("Lucky");
		
		one.start();
		two.start();
		
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		for (int i = 0; i < 5; i++) {
			makeWithdrawl(10);
			if(acct.getBalance() < 0)
			{
				System.out.println("Account is overdrawn !");
			}
			
		}
	}
	
	private synchronized void makeWithdrawl(int amount)
	{
		if(acct.getBalance() >= amount)
		{
			System.out.println("Thread " + Thread.currentThread().getName() + " is going to withdraw");
			try {
				Thread.sleep(500);
				
			} catch (InterruptedException ie) {
				// TODO: handle exception
				ie.printStackTrace();
			}
			acct.withdraw(amount);
			System.out.println("Thread " + Thread.currentThread().getName() + " completes the withdrawl");
			
		}
		else {
			System.out.println("Not enough balane in account for " + Thread.currentThread().getName() + " to withdraw " + acct.getBalance());
		}
	}
}

class Account{
	private int balance = 50;
	
	public int getBalance()
	{
		return balance;
	}
	
	public void withdraw(int amount)
	{
		balance = balance - amount;
	}
	
}



