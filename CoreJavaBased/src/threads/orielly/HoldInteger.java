package threads.orielly;

import java.util.Date;

public class HoldInteger {
	private boolean empty = true;
	private int sharedInt = 0;
	
	public synchronized void  setSharedInt(int val)
	{
		while(!empty)
		{
			try
			{
				wait();
			}
			catch(InterruptedException ex)
			{
				System.out.println(ex.getMessage());
			}
		}
		sharedInt = val;
		System.out.println( new Date() + " in setSharedInt(): " + sharedInt);
		empty = false;
		notify();
	}
	
	public synchronized int getSharedInt()
	{
		while(empty)
		{
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		empty = true;
		System.out.println( new Date() + " in getSharedInt(): " + sharedInt);
		notify();
		return sharedInt;
	}
	
}
