package threads.orielly;

public class SharedCell {
	public  static void main(String[] args)
	{
		HoldInteger h = new HoldInteger();
		
		Producer p = new Producer(h);
		Consumer c = new Consumer(h);
		
		p.start();
		c.start();
	}
}
