package threads.orielly;

import java.util.Date;

public class Producer extends Thread{
	private HoldInteger pHold;
	
	public Producer(HoldInteger hi)
	{
		pHold = hi;
	}
	
	public void run()
	{
		for (int i = 0; i < 10; i++) {
			pHold.setSharedInt(i);
			System.out.println(new Date() + "Producer set sharedInt: " + i);
		}
		//sleep for a Random interval
		try
		{
			sleep((int)(Math.random()*1000));
		}
		catch(InterruptedException e)
		{
			System.out.println(e.getMessage());
		}
	}
}
