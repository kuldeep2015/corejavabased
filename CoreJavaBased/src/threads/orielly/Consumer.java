package threads.orielly;

import java.util.Date;

public class Consumer extends Thread{
	private HoldInteger cHold;
	public Consumer(HoldInteger h)
	{
		cHold = h;
	}
	
	public void run()
	{
		int val;
		val = cHold.getSharedInt();
		System.out.println(new Date() + "Consumer retrieved val: " + val);
		while(val != 9)
		{
			//sleep for a Random interval
			try
			{
				sleep((int)(Math.random()*1000));
			}
			catch(InterruptedException e)
			{
				System.out.println(e.getMessage());
			}
			val = cHold.getSharedInt();
			System.out.println(new Date() + "Consumer retrived val: " + val);
		}
	}
}
