package threads.example2;

import java.awt.Component;

public class TimerThread extends Thread{
	Component comp;
	int timediff;
	volatile boolean shutRun;
	
	public TimerThread(Component comp, int timediff)
	{
		this.comp = comp;
		this.timediff = timediff;
		shutRun = true;
	}
	
	public void run()
	{
		while(shutRun)
		{
			comp.repaint();
			try {
				sleep(timediff);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
