package threads.example2;

import java.applet.Applet;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MediaTracker;

public class Animate extends Applet {
	
	int count,lastCount;
	Image pictures[];
	TimerThread timer;
	String imagePath = "C://Documents and Settings/rajeev/Desktop/Bittu_Photos/Pic";
	
	public void init()
	{
		lastCount = 10;
		count = 0;
		pictures = new Image[10];
		MediaTracker tracker = new MediaTracker(this);
		
		for (int i = 0; i < lastCount; i++) {
			pictures[i] = getImage(getCodeBase(), "12012011.JPEG");
			tracker.addImage(pictures[i], 0);
		}
		tracker.checkAll(true);
	}
	
	public void start()
	{
		timer = new TimerThread(this, 1000);
		timer.start();
	}
	
	public void stop()
	{
		timer.shutRun = false;
		timer = null;
	}
	
	public void paint(Graphics g)
	{
		g.drawImage(pictures[count++], 0, 0, null);
	}
}



