package enums;

import java.util.logging.Logger;

public class EnumsExecutor {


	private static final Logger LOGGER = Logger.getLogger(EnumsExecutor.class.getName());
	
	public static void main(String[] args) {
		MonthTypeEnum monthTypeEnum = MonthTypeEnum.JANUARY;
		
		LOGGER.info("name(): " + monthTypeEnum.name());
		LOGGER.info("valueOf(): " + monthTypeEnum.valueOf(MonthTypeEnum.JANUARY.toString()));
	}
}
