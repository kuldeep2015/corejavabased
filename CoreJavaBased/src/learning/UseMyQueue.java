package learning;

public class UseMyQueue {
	
	MyQueue<String> myQueue1 = null;
	MyQueue<String> myQueue2 = null;
	
	public static void main(String[] args)
	{
		UseMyQueue useMyQueue = new UseMyQueue();
		useMyQueue.perform(MyQueue.QueueType.FIFO );
		useMyQueue.perform(MyQueue.QueueType.LIFO );
	}

	private void perform(MyQueue.QueueType queueType) {
		
		if(queueType.equals(MyQueue.QueueType.LIFO))
		{
			myQueue1 = new QueueLIFO<String>();
			System.out.println("Performing operations for LIFO queue.");
			addToQueue(myQueue1);
			removeFromQueue(myQueue1);
			
		}
		else if(queueType.equals(MyQueue.QueueType.FIFO))
		{
			myQueue2 = new QueueFIFO<String>();
			System.out.println("Performing operations for FIFO queue.");
			addToQueue(myQueue2);
			removeFromQueue(myQueue2);
		}	
	}
	
	private void addToQueue(MyQueue<String> myQueue)
	{
		System.out.println("Adding to Queue");
		myQueue.add(CountStringType.ONE.toString());
		myQueue.showQueueElements();
		myQueue.add(CountStringType.TWO.toString());
		myQueue.showQueueElements();
		myQueue.add(CountStringType.THREE.toString());
		myQueue.showQueueElements();
		myQueue.add(CountStringType.FOUR.toString());
		myQueue.showQueueElements();
		myQueue.add(CountStringType.FIVE.toString());
		myQueue.showQueueElements();
		myQueue.add(CountStringType.SIX.toString());
		myQueue.showQueueElements();
		myQueue.add(CountStringType.SEVEN.toString());
		myQueue.showQueueElements();
		myQueue.add(CountStringType.EIGHT.toString());
		myQueue.showQueueElements();
		myQueue.add(CountStringType.NINE.toString());
		myQueue.showQueueElements();
		myQueue.add(CountStringType.TEN.toString());
		myQueue.showQueueElements();
			
	}
	
	
	
	private void removeFromQueue(MyQueue<String> myQueue)
	{
		System.out.println("Removing from Queue");
		while(myQueue.getQueueSize() > 0)
		{
			myQueue.remove();
			myQueue.showQueueElements();
		}
	}

	
}
