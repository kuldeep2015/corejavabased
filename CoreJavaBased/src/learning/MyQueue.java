package learning;

import java.util.LinkedList;
import java.util.List;

/**
 * This class is designed for implementation of Queue 
 * data structure according LIFO/FIFO implementation. 
 * @author Kuldeep
 *
 * @param <E>
 */
public abstract class MyQueue<E> {
	
	LinkedList<E> myList = new LinkedList<E>();
	enum QueueType{
		LIFO,FIFO
	}
	
	/**
	 * Abstract method left for further implementation.
	 * @param myElement
	 */
	public abstract void add(E myElement);
	
	/**
	 * Abstract method left for further implementation.
	 * @return
	 */
	public abstract E get();
	
	/**
	 * 
	 * @return
	 */
	public abstract  E getFirstElement();
	
	/**
	 * 
	 * @return
	 */
	public abstract  E getLastElement();
	
	public abstract void remove();
	
	/**
	 * Abstract method left for further implementation
	 * @return
	 */
	public abstract E remove(int index);
	
	/**
	 * 
	 * @return
	 */
	public final E removeFirst()
	{
		return myList.removeFirst();
	}
	
	public final E removeLast()
	{
		return myList.removeFirst();
	}
	
	/**
	 * 
	 */
	public final void showQueueElements()
	{
		System.out.println(myList.toString());
	}
	/**
	 * 
	 * @return
	 */
	public final int getQueueSize()
	{
		return myList.size();
	}
}
