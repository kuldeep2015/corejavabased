package learning;

/**
 * This class is LIFO representation of Queue data structure.
 * The element will be added/removed from the top only.
 * @author Kuldeep
 *
 * @param <E>
 */
public class QueueLIFO<E> extends MyQueue<E> {

	/**
	 * 
	 */
	@Override
	public void add(E myElement) {
		// TODO Auto-generated method stub
		myList.addLast(myElement);
	}
	
	/**
	 * 
	 */
	@Override
	public E get() {
		// TODO Auto-generated method stub
		return myList.getLast();
	}
	
	/**
	 * 
	 */
	@Override
	public E getFirstElement() {
		// TODO Auto-generated method stub
		E element = myList.getFirst();
		return element;
	}
	/**
	 * 
	 */
	@Override
	public E getLastElement() {
		// TODO Auto-generated method stub
		E element = myList.getLast();
		return element;
	}
	
	/**
	 * 
	 */
	@Override
	public E remove(int index) {
		// TODO Auto-generated method stub
		return myList.remove(index);
	}

	@Override
	public void remove() {
		// TODO Auto-generated method stub
		myList.removeLast();
	}
}
