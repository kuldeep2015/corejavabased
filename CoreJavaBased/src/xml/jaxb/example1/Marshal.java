package xml.jaxb.example1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class Marshal {

	private static final String FILE_PATH = "E:/workspace/CoreJavaBased/src/xml/jaxb/example1";
	private static final String file_name = "bd.mar";
	
	public static void main(String[] args)
	{
		Marshal marshal = new Marshal();
		marshal.run_example();
	}
	
	private void run_example()
	{
		try {
			JAXBContext ctx = JAXBContext.newInstance(Skier.class);
			Marshaller m = ctx.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			
			//Marshal a Skier object to stdout
			Skier skier = createSkier();
			m.marshal(skier, System.out);
			//Marshal a Skier object to file
			FileOutputStream out = new FileOutputStream(FILE_PATH + "/" + file_name);
			m.marshal(skier, out);
			
			out.close();
			
			//UnMarshal as proof of concept
			Unmarshaller u = ctx.createUnmarshaller();
			Skier bd_clone = (Skier) u.unmarshal(new File(FILE_PATH + "/" + file_name));
			System.out.println();
			m.marshal(bd_clone, System.out);
			
			
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	private Skier createSkier()
	{
		Person person = new Person("Kuldeep", 30, "Male");
		List<String> list = new ArrayList<String>();
		list.add("12 Olympic Medals");
		list.add("9 World Championships");
		list.add("Winningest Winter Olympian");
		list.add("Greatest Nordic Skier");
		
		return new Skier(person, "India", list); 
	}
}
