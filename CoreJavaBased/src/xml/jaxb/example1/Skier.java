package xml.jaxb.example1;

import java.util.Collection;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Skier {
	
	//fields
	private Person person;
	private String national_team;
	private Collection major_achievements;
	
	//Constructors
	public Skier() {}
	
	public Skier(Person person, String national_team, Collection<String> major_achievements)
	{
		setPerson(person);
		setNational_team(national_team);
		setMajor_achievements(major_achievements);
		
	}
	
	public void setPerson(Person person) {
		this.person = person;
	}
	public Person getPerson() {
		return person;
	}
	
	public void setNational_team(String national_team) {
		this.national_team = national_team;
	}
	public String getNational_team() {
		return national_team;
	}
	
	public void setMajor_achievements(Collection major_achievements) {
		this.major_achievements = major_achievements;
	}
	public Collection getMajor_achievements() {
		return major_achievements;
	}
}
