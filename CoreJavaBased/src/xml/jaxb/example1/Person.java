package xml.jaxb.example1;

import javax.xml.bind.annotation.XmlType;

@XmlType
public class Person {
	
	//Fields
	private String name;
	private int age;
	private String gender;
	
	//Constructor
	public Person()
	{}
	
	public Person(String name, int age, String gender)
	{
		setName(name);
		setAge(age);
		setGender(gender);
	}
	
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	
	public void setAge(int age) {
		this.age = age;
	}
	public int getAge() {
		return age;
	}
	
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getGender() {
		return gender;
	}
	
	
	
	
}


