package httpURLConnectionExample;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;



public class HttpClientExecuter {

	private static String SERVER_URL = "http://yamunaexpresswayauthority.com/resbhs1.php";
	//"?id=25369";
	private final String USER_AGENT = "Mozilla/5.0";
	
	public static void main(String[] args)
	{
		try {
			new HttpClientExecuter().sendPost();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// HTTP POST request
		private void sendPost() throws Exception {
	 
			HttpClient client = null;
			HttpPost post = null;
			BufferedReader rd = null;
			try
			{
			
				client = new DefaultHttpClient();
				post = new HttpPost( SERVER_URL );
		 
				// add header
				post.setHeader("User-Agent", USER_AGENT);
		 
				List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
				urlParameters.add(new BasicNameValuePair("sn", "C02G8416DRJM"));
				urlParameters.add(new BasicNameValuePair("cn", ""));
				urlParameters.add(new BasicNameValuePair("locale", ""));
				urlParameters.add(new BasicNameValuePair("caller", ""));
				urlParameters.add(new BasicNameValuePair("id", "25377"));
		 
				post.setEntity(new UrlEncodedFormEntity(urlParameters));
		 
				HttpResponse response = client.execute(post);
				System.out.println("\nSending 'POST' request to URL : " + SERVER_URL);
				System.out.println("Post parameters : " + post.getEntity());
				System.out.println("Response Code : " +  response.getStatusLine().getStatusCode());
		 
				rd = new BufferedReader( new InputStreamReader(response.getEntity().getContent()));
		 
				StringBuilder result = new StringBuilder();
				String line = "";
				while ((line = rd.readLine()) != null) {
					result.append(line);
				}
		 
				System.out.println(result.toString());
	 
			}
			finally
			{
				
			}
		}
	
	void executeProcess() throws IOException
	{
		URL myUrl = null;
		HttpURLConnection myConnection = null;
		BufferedReader bufferedReader = null;
		OutputStreamWriter writer = null;
		try
		{
		
			myUrl = new URL(SERVER_URL);
	
			myConnection = (HttpURLConnection) myUrl.openConnection();
			myConnection.setRequestMethod("POST");
			myConnection.setRequestProperty("User-Agent", USER_AGENT);
			myConnection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
			//myConnection.addRequestProperty("id", "25377");
			myConnection.setDoOutput(true);
	
			//myConnection.connect();
			
			writer = new OutputStreamWriter(myConnection.getOutputStream ());
			
			writer.write("id=25377");
			writer.flush();
			// code continues to read the response stream
			
			bufferedReader = new BufferedReader( new InputStreamReader( myConnection.getInputStream()) ) ;
			String readLine = null;
			StringBuilder stringBuilder = new StringBuilder();
			while (  (readLine = bufferedReader.readLine() ) != null )
			{
				stringBuilder.append(readLine);
			}
			
			// Read the data now printing it.
			System.out.println(stringBuilder);
		}
		finally
		{
			if( bufferedReader != null )
			{
				bufferedReader.close();
			}
			if(writer != null)
			{
				writer.close();
			}
			if(myConnection != null )
			{
				myConnection.disconnect();
			}
			
			System.out.println("Connection closed.");
		}
		
	}
}
