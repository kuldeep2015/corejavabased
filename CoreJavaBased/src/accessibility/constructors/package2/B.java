package accessibility.constructors.package2;

import accessibility.constructors.package1.A;

public class B extends A {

	int val;
	public B() {
		super();
	}
	
	public B(int val){
		this();
		this.val = val;
		
	}
	
	
	public static void main(String[] args) {
	
		B b = new B();
		
		
	}
}
