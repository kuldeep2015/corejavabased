package headFirstJava.dotComGame;

import java.util.ArrayList;

public class DotCom {
	private ArrayList<String> locationCells;
	private String name;
	
	public void setLocationCells(ArrayList<String> loc) {
		this.locationCells = loc;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String checkYourself(String userInput)
	{
		String result = "miss";
		int index = locationCells.indexOf(userInput);
		if(index >= 0)
		{
			locationCells.remove(index); // delete an entry
			if(locationCells.isEmpty())
			{
				result = "Kill";
				System.out.println("Ouch you sunk " + name);
			}
			else
			{
				result = "Hit";
			} //close if
		}	//close if
		return result;
	}	//close method
}	// close Class
