package innerClasses.anonymousInnerClass.example1;

public class Food {

	Popcorn p = new Popcorn(){
		
		public void pop()
		{
			System.out.println("Anonymous Popcorn");
			super.pop();
		}
	};
	
	public void showPopcorn()
	{
		p.pop();
	}
	
	public static void main(String[] args)
	{
		Food food = new Food();
		food.showPopcorn();
	}
}

class Popcorn{
	public void pop()
	{
		System.out.println("popcorn");
	}
}
