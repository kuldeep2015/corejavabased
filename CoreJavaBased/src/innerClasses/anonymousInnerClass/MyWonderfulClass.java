package innerClasses.anonymousInnerClass;

public class MyWonderfulClass {
	
	void go()
	{
		Bar b = new Bar();
		b.doStuff(
			new Foo(){
				public void foof()
				{
					System.out.println("foofy");
				}
			});
	}
	
	public static void main(String[] args)
	{
		new MyWonderfulClass().go();
	}
}

class Bar{
	
	void doStuff(Foo foo)
	{
		foo.foof();
	}
}


interface Foo
{
	void foof();
}

