package innerClasses.regularInner.example1;

public class MyOuter {
	
	private int x = 7;
	
	public static void main(String[] args)
	{
		new MyOuter().makeInner();
	}
	
	public void makeInner()
	{
		MyInner inner = new MyInner(); // Make an inner instance
		inner.seeOuter();
	}
	
	public class MyInner
	{
		public void seeOuter()
		{
			System.out.println("Outer x is: " + x);
			System.out.println("Inner class ref is: " + this);
			System.out.println("Outer class ref is: " + MyOuter.this);
			
			
			
			
		}
	}
}
