package innerClasses.regularInner.example2;

import innerClasses.regularInner.example1.MyOuter;

public class UseMyOuterMyInner {
	
	public static void main(String[] args)
	{
		MyOuter mo = new MyOuter(); // gotta get an instance
		MyOuter.MyInner inner = mo.new MyInner();
		inner.seeOuter();
		
	}
}
