package collections.sorting.dvdDemo;

import java.util.List;

public class DvdUtility {
	
	/**
	 * @author Kuldeep
	 * @param dvdList
	 */
	public static void populateList(List<DVDInfo> dvdList)
	{
		DVDInfo dvdInfo = new DVDInfo("Donnie Darko","sci-fi","Gyllenhall, Jake");
		dvdList.add(dvdInfo);
		dvdInfo = new DVDInfo("Raiders of the lost Ark","action","Ford, Harrison");
		dvdList.add(dvdInfo);
		dvdInfo = new DVDInfo("2001","sci-fi","James, Mathew");
		dvdList.add(dvdInfo);
		dvdInfo = new DVDInfo("Caddy Shack","comedy","Murray, Bill");
		dvdList.add(dvdInfo);
		dvdInfo = new DVDInfo("Star Wars","sci-fi","Ford, Harrison");
		dvdList.add(dvdInfo);
		dvdInfo = new DVDInfo("Lost in Translation","comedy","Murray, Bill");
		dvdList.add(dvdInfo);
		dvdInfo = new DVDInfo("Patriot Games","action","Ford, Harrison");
		dvdList.add(dvdInfo);
		
	}

}
