package collections.sorting.dvdDemo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TestDvdInfo {
	
	public static void main(String[] args)
	{
		TestDvdInfo testDvdInfo = new TestDvdInfo();
		testDvdInfo.start();
	}
	
	public void start()
	{
		List<DVDInfo> dvdInfoList = new ArrayList<DVDInfo>();
		DvdUtility.populateList(dvdInfoList);
		
		System.out.println("Original List:");
		System.out.println(dvdInfoList);
		
		Collections.sort(dvdInfoList, new DvdTitleCompareImpl());
		System.out.println("Sorting by Title:");
		System.out.println(dvdInfoList);
		
		Collections.sort(dvdInfoList, new DvdGenreCompareImpl());
		System.out.println("Sorting by Genre:");
		System.out.println(dvdInfoList);
		
		Collections.sort(dvdInfoList, new DvdLeadActorCompareImpl());
		System.out.println("Sorting by LeadActor:");
		System.out.println(dvdInfoList);
	}
	

}
