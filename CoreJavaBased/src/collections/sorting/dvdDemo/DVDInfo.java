package collections.sorting.dvdDemo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DVDInfo implements Comparable<DVDInfo> {
	String title;
	String genre;
	String leadActor;
	
	public DVDInfo(String t, String g, String a) {
		title = t;
		genre = g;
		leadActor = a;
	}
	
	public String toString()
	{
		return title + " "+ genre + " " + leadActor + "\n";
	}

	public void setTitle(String title) {
		this.title = title;
	}
	public String getTitle() {
		return title;
	}
	
	
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public String getGenre() {
		return genre;
	}
	
	public void setLeadActor(String leadActor) {
		this.leadActor = leadActor;
	}
	public String getLeadActor() {
		return leadActor;
	}
	
	public static void main(String[] args)
	{
		List<DVDInfo> dvdList = new ArrayList<DVDInfo>();
		DvdUtility.populateList(dvdList);
		System.out.println(dvdList);
		Collections.sort(dvdList);
		System.out.println(dvdList);
	}
	
//	public static void populateList(List<DVDInfo> dvdList)
//	{
//		DVDInfo dvdInfo = new DVDInfo("Donnie Darko","sci-fi","Gyllenhall, Jake");
//		dvdList.add(dvdInfo);
//		dvdInfo = new DVDInfo("Raiders of the lost Ark","action","Ford, Harrison");
//		dvdList.add(dvdInfo);
//		dvdInfo = new DVDInfo("2001","sci-fi","??");
//		dvdList.add(dvdInfo);
//		dvdInfo = new DVDInfo("Caddy Shack","comedy","Murray, Bill");
//		dvdList.add(dvdInfo);
//		dvdInfo = new DVDInfo("Star Wars","sci-fi","Ford, Harrison");
//		dvdList.add(dvdInfo);
//		dvdInfo = new DVDInfo("Lost in Translation","comedy","Murray, Bill");
//		dvdList.add(dvdInfo);
//		dvdInfo = new DVDInfo("Patriot Games","action","Ford, Harrison");
//		dvdList.add(dvdInfo);
//		
//	}

	@Override
	public int compareTo(DVDInfo o) {
		// TODO Auto-generated method stub
		return title.compareTo(o.getTitle());
	}
}
