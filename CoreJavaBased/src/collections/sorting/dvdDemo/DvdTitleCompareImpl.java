package collections.sorting.dvdDemo;

import java.util.Comparator;

public class DvdTitleCompareImpl implements Comparator<DVDInfo>{
	
	@Override
	public int compare(DVDInfo o1, DVDInfo o2) {
		// TODO Auto-generated method stub
		return o1.getTitle().compareTo(o2.getTitle());
	}

}
