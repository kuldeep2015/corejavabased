package collections.sorting;

import java.util.ArrayList;
import java.util.Collections;

/**
 * 
 * @author Kuldeep
 * Example of natural sorting of String objects
 */
public class TestSort1 {
	public static void main(String[] args)
	{
		ArrayList<String> stuf = new ArrayList<String>();
		stuf.add("Denver");
		stuf.add("Boulder");
		stuf.add("Vail");
		stuf.add("Aspen");
		stuf.add("Telluride");
		System.out.println("Unsorted stuff: " + stuf);
		long startTime,endTime;
		startTime = System.currentTimeMillis();
		Collections.sort(stuf);
		endTime = System.currentTimeMillis();
		System.out.println("Sorted stuff: " + stuf);
		System.out.println("Time taken in Sorting: " + (endTime - startTime) + " MilliSeconds");
	}
}
