package collections.sorting.comparator;

import java.io.Serializable;
import java.util.Comparator;

public class PetComparator implements Comparator, Serializable {

	@Override
	public int compare(Object o1, Object o2) {
		// TODO Auto-generated method stub
		
		Pet pet = (Pet)o1;
		Pet petAnother = (Pet)o2;
		
		// Use Integer class's natural ordering
		Integer pId = new Integer(pet.getPetId());
		Integer pAnotherId = new Integer(petAnother.getPetId());
		
		int result = pId.compareTo(pAnotherId);
		
		// id's are same compare by petType
		if(result == 0)
		{
			String pet1Type = pet.getPetType();
			String petAnotherType = petAnother.getPetType();
			
			result = pet1Type.compareTo(petAnotherType);
		}
		
		return result;
	}

}
