package collections.sorting.comparator;

public class Pet {
	
	int petId;
	String petType;
	
	public Pet(int petId, String petType)
	{
		this.petId = petId;
		this.petType = petType;
	}

	public void setPetId(int petId) {
		this.petId = petId;
	}
	
	public int getPetId() {
		return petId;
	}
	
	
	public void setPetType(String petType) {
		this.petType = petType;
	}
	public String getPetType() {
		return petType;
		
	}
}
