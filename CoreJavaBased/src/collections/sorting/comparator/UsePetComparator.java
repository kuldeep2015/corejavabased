package collections.sorting.comparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;


public class UsePetComparator {
	
	public static void main(String[] args)
	{
		UsePetComparator usePetComparator =  new UsePetComparator();
		usePetComparator.doByList();
		usePetComparator.doByTreeSet();
	}
	
	private void doByList()
	{
		System.out.println("Do by List -->");
		List list = new ArrayList();
		list.add(new Pet(2,"Dog"));
		list.add(new Pet(3,"Cat"));
		list.add(new Pet(1,"Parrot"));
		
		Collections.sort(list, new PetComparator());
		
		for (Iterator iterator = list.iterator(); iterator.hasNext();) {
			Pet element = (Pet) iterator.next();
			System.out.println(element.getPetType());
			
		}
		
	}
	
	private void doByTreeSet()
	{
		System.out.println("Do By TreeSet -->");
		
		Set treeSet = new TreeSet(new PetComparator());
		
		treeSet.add(new Pet(2,"Dog"));
		treeSet.add(new Pet(3,"Cat"));
		treeSet.add(new Pet(1,"Parrot"));
		
		for (Iterator iterator = treeSet.iterator(); iterator.hasNext();) {
			Pet pet = (Pet) iterator.next();
			System.out.println(pet.getPetType());
		}
	}
}
