package annotations.interfaceExample;

import java.lang.reflect.Method;

@MyParentObject (isInherited = true, doSomething = "From Class UseMyChildObject")
public class UseMyChildObject {

	
	public static void main(String[] args) throws NoSuchMethodException, SecurityException
	{
		MyChildObject myChildObject = new MyChildObject();
		Class c = myChildObject.getClass();
		MyParentObject  myParentObject = ( MyParentObject ) c.getAnnotation(MyParentObject.class);
		System.out.println("myParentObject.doSomething(): " + myParentObject.doSomething());
		System.out.println("myParentObject.isInherited(): " + myParentObject.isInherited());
		
		Class c1 = UseMyChildObject.class;
		MyParentObject useMyChildObject = (MyParentObject) c1.getAnnotation(MyParentObject.class);
		System.out.println("useMyChildObject.doSomething(): " + useMyChildObject.doSomething());
		System.out.println("useMyChildObject.isInherited(): " + useMyChildObject.isInherited());
		
		
		UseMyChildObject useMyChildObject2 = new UseMyChildObject();
		
		Class class2 =  UseMyChildObject.class;
		Method method = class2.getMethod("test");
		MyParentObject useMyChildObject3 = (MyParentObject) method.getAnnotation(MyParentObject.class);
		System.out.println("useMyChildObject.doSomething(): " + useMyChildObject3.doSomething());
		System.out.println("useMyChildObject.isInherited(): " + useMyChildObject3.isInherited());
	}
	
	@MyParentObject (isInherited = false, doSomething = "From Class UseMyChildObject test() method")
	public static void test()
	{
		
	}
}
