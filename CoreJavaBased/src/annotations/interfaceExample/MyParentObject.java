package annotations.interfaceExample;


import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface MyParentObject {
	public boolean isInherited() default true;
	public String doSomething() default "Do What";
	
}
