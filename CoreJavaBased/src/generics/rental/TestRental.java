package generics.rental;

import java.util.ArrayList;
import java.util.List;

public class TestRental {
	
	public static void main(String[] args)
	{
		Car c1 = new Car();
		Car c2 = new Car();
		
		List<Car> carList = new ArrayList<Car>();
		carList.add(c1);
		carList.add(c2);
		
		RentalGeneric<Car> carRental = new RentalGeneric<Car>(2, carList);
		Car carToRent = carRental.getRental();
		System.out.println("Car hired on rent:" + carToRent);
		carRental.returnRental(carToRent);
		System.out.println("Car hired, returned to pool:" + carToRent);
		
		
	}
}
