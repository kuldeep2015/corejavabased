package generics.animal;

public class AnimalHolder<T extends Animal> {
	T animal;
	
	public AnimalHolder(T animal) {
		// TODO Auto-generated constructor stub
		this.animal = animal;
	}
	
	public static void main(String[] args)
	{
		AnimalHolder<Dog> dogHolder = new AnimalHolder<Dog>(new Dog());
		dogHolder.getAnimal().checkUp();
		
		AnimalHolder<Animal> animalHolder = new AnimalHolder<Animal>(new Bird());
		animalHolder.getAnimal().checkUp();
	}
	
	T getAnimal()
	{
		return animal;
	}
	
	
}
