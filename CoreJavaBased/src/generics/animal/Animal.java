package generics.animal;

abstract class Animal {
	
	public abstract void checkUp();
}

class Dog extends Animal
{
	@Override
	public void checkUp() {
		// TODO Auto-generated method stub
		System.out.println("Dog CheckedUp");
	}
}

class Cat extends Animal
{
	@Override
	public void checkUp() {
		// TODO Auto-generated method stub
		System.out.println("Cat CheckedUp");
	}
}

class Bird extends Animal
{
	@Override
	public void checkUp() {
		// TODO Auto-generated method stub
		System.out.println("Bird CheckedUp");
	}
}