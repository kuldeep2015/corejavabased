package generics.animal;

public class AnimalDoctor {
	
	public void checkAnimals(Animal[] animals)
	{
		for (Animal animal : animals) {
			animal.checkUp();
		}
	}
	
	public static void main(String[] args)
	{
		Dog[] dogs = {new Dog(), new Dog()};
		Cat[] cats = {new Cat(), new Cat(), new Cat()};
		Bird[] birds = {new Bird(), new Bird()};
		
		AnimalDoctor animalDoctor = new AnimalDoctor();
		animalDoctor.checkAnimals(dogs);
		animalDoctor.checkAnimals(cats);
		animalDoctor.checkAnimals(birds);
		
		
	}
}
