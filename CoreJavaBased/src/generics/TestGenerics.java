package generics;

public class TestGenerics<T> {
	T anInstance;
	T[] anArraysOfTs;
	
	TestGenerics(T anInstance)
	{
		this.anInstance = anInstance;
	}
	
	T getT()
	{
		return anInstance;
	}
}
