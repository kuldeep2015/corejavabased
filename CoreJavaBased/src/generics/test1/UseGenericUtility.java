package generics.test1;

public class UseGenericUtility {
	
	public static void main(String[] args)
	{
		GenericUtility<String, Integer, Boolean> genericUtility = new GenericUtility<String, Integer, Boolean>( new GenericUtility<String, Integer, Boolean>() );
		genericUtility.setObjectValues("Kuldeep", 100, true);
		
		GenericUtility<String, Integer, Boolean> genericUtility2 = genericUtility.getObject();
		System.out.println("genericUtility2.getX(): " + genericUtility2.getX() + " genericUtility2.getY(): " + genericUtility2.getY() + " genericUtility2.getZ(): " +  genericUtility2.getZ());
	}
}
