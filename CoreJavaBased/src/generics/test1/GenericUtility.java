package generics.test1;

import java.util.List;

public class GenericUtility<X,Y,Z> {
	
	GenericUtility<X, Y, Z> genericUtility;
	private X x;
	private Y y;
	private Z z;
	
	public GenericUtility() {
		// TODO Auto-generated constructor stub
		super();
	}
	
	GenericUtility(GenericUtility<X, Y, Z> genericUtility )
	{
		this.genericUtility = genericUtility;
		this.x = genericUtility.getX();
		this.y = genericUtility.getY();
		this.z = genericUtility.getZ();
	}
	
	public X getX()
	{
		return x;
	}
	
	public Y getY()
	{
		return y;
	}
	
	public Z getZ()
	{
		return z;
	}
	
	GenericUtility<X, Y, Z> getObject()
	{
		return genericUtility;
	}
	
	void setObject(GenericUtility<X, Y, Z> genericUtility)
	{
		this.genericUtility = genericUtility;
	}
	
	void setObjectValues(X x, Y y, Z z)
	{
		genericUtility.x = x;
		genericUtility.y = y;
		genericUtility.z = z;
	}
	
	List<GenericUtility<X, Y, Z>> addAndGetList(GenericUtility<X, Y, Z> genericUtility, List<GenericUtility<X, Y, Z>> geneList)
	{
		geneList.add(genericUtility);
		return geneList;
	}
}
