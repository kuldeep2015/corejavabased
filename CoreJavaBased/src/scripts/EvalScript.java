package scripts;
import javax.script.*;



public class EvalScript {

	public static void main(String[] args) throws ScriptException
	{
		//Create a script engine manager
		ScriptEngineManager factory = new ScriptEngineManager();
		ScriptEngine engine = factory.getEngineByName("JavaScript");
		//Evaluate java script code from String
		//engine.eval("print('Hello, World')");
		engine.eval("print('Hello, World\\r\\n');");
		engine.eval("showPrint(); function showPrint(){ print('Hi Alert');}");
		
		
	}
}
